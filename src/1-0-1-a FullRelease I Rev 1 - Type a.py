import pygame,sys
from pygame.locals import *

# Placeholder Resolution

FPS = 100

def main():
    global fpsClock, DISPLAYSURF
    pygame.init()
    fpsClock = pygame.time.Clock()
    DISPLAYSURF = pygame.display.set_mode((0,0), pygame.FULLSCREEN)
    screenX , screenY = DISPLAYSURF.get_size()
    DISPLAYSURF.fill(( 0, 0, 0)) 
    pygame.key.set_repeat(10)
    
    posX = round(screenX/2)
    posY = round(screenY/2)
    countsec = 100
    second = 0
    minute = 0
    hour = 0
    start = 0
    hold = 0
    countup = 1
    blink = 0
    select = 2 # 1 = Hour, 2 = Minute, 3 = Second
    
    timer_event = pygame.USEREVENT + 1
    pygame.time.set_timer(timer_event,1)
    while True:
        pygame.display.set_caption('Timer')
        DISPLAYSURF.fill(( 0, 0, 0)) 
        for event in pygame.event.get():
            
            # Get Quit Signal
            if event.type==QUIT:
                pygame.quit()
                sys.exit()
                
            # Get User Input
            elif event.type == KEYUP:
            	if select == 0 and blink == 1:
            	    select = 2
            	    blink = 0
            	blink = 0
            	hold = 0
                if event.key == K_ESCAPE:
                    pygame.quit()
                    sys.exit()
            	if event.key == K_RIGHT:
                    select = select + 1
                    if select > 3:
                    	select = 2
                if event.key == K_LEFT:
                    select = select - 1
                    if select < 2:
                    	select = 3
                if event.key == K_UP:
                    if select == 1 and hour < 99:
                        hour = hour + countup
                        if hour > 99:
                            hour = 99
                    elif select == 2 and minute < 59:
                    	minute = minute + countup
                    	if minute > 59:
                    	    minute = 59
                    elif select == 3 and second < 59:
                    	second = second + countup
                    	if second > 59:
                    	    second = 59
                if event.key == K_DOWN:
                    if select == 1 and hour > 0:
                        hour = hour - countup
                        if hour < 0:
                            hour = 0
                    elif select == 2 and minute > 0:
                    	minute = minute - countup
                    	if minute < 0:
                    	    minute = 0
                    elif select == 3 and second > 0:
                    	second = second - countup
                    	if second < 0:
                    	    second = 0
                if event.key == K_SPACE:
                    if blink == 1:
                    	blink = 0
                    	select = 2
	            elif start == 0:
                    	start = 1
                    elif start == 1:
                        start = 0
                    countsec = 60
                if event.key == K_RETURN:
                    if blink == 1:
                    	blink = 0
                    	select = 2
	            elif start == 0:
                    	start = 1
                    elif start == 1:
                        start = 0
                    countsec = 60
                    	
            elif event.type == KEYDOWN:
            	hold = hold + 1
            	if select == 0 and blink == 1:
           	    blink = 0
            	if hold >= 20:
                    countup = 10
#                if event.key == K_UP:
#                    if select == 1 and hour < 99:
#                        hour = hour + countup
#                        if hour > 99:
#                            hour = 99
#                    elif select == 2 and minute < 59:
#                    	minute = minute + countup
#                    	if minute > 59:
#                    	    minute = 59
#                    elif select == 3 and second < 59:
#                    	second = second + countup
#                    	if second > 59:
#                    	    second = 59
#                if event.key == K_DOWN:
#                    if select == 1 and hour > 0:
#                        hour = hour - countup
#                        if hour < 0:
#                            hour = 0
#                    elif select == 2 and minute > 0:
#                    	minute = minute - countup
#                    	if minute < 0:
#                    	    minute = 0
#                    elif select == 3 and second > 0:
#                    	second = second - countup
#                    	if second < 0:
#                    	    second = 0
            elif start == 1 and event.type == timer_event:
            	countsec = countsec
            countup = 1
                    	
            if start == 0:
            	countsec = countsec - 1
            	if countsec <= 0:
                    countsec = 60
            if event.type == timer_event and start == 1:
            	select = 0
            	blink = 0
            	countsec = countsec - 1
            	if countsec <= 0:
                    second = second - 1
                    countsec = FPS
                    if second <= -1:
                        if (hour <= 0) & (minute <= 0) & (second <= 0):
                            blink = 1
                            start = 0
                            second = 0
                            minute = 0
                            hour = 0
                            countsec = FPS
                        else:
                            second = 59
                            minute = minute - 1
                    	    if minute <= -1:
                                hour = hour - 1
                                minute = 59
                                if (hour <= 0) & (minute <= 0) & (second <= 0):
                                    blink = 1
                                    start = 0
             
                    
                        
        showHour = "%02.0f" % (hour%60)
        showMinute = "%02.0f" % (minute%60)
        showSecond = "%02.0f" % (second%60)
        
        print("Tick")
        # Interface
        
        fontObj = pygame.font.Font('OpenSans-Light.ttf', 60)
        textSurfaceObj = fontObj.render("Paused", True, [255, 255, 255])
        textRectObj = textSurfaceObj.get_rect()
        textRectObj.center = [screenX / 2, screenY / 2 + 200]
        if (start == 0):
            DISPLAYSURF.blit(textSurfaceObj, textRectObj)
            
        fontObjA = pygame.font.Font('OpenSans-Light.ttf', 40)
        textSurfaceObjA = fontObjA.render("Press Enter/Space To Start/Continue", True, [255, 255, 255])
        textRectObjA = textSurfaceObjA.get_rect()
        textRectObjA.center = [screenX / 2, screenY / 2 + 250]
        if (start == 0):
            DISPLAYSURF.blit(textSurfaceObjA, textRectObjA)
        
#        fontObj1 = pygame.font.Font('Capsuula.ttf', 150)
#        textSurfaceObj1 = fontObj1.render(showHour, True, [255, 255, 255])
#        textRectObj1 = textSurfaceObj1.get_rect()
#        textRectObj1.center = [screenX / 2 - 180, screenY / 2]
#        if (blink == 1) or (select == 1 and start == 0):
#            if countsec % 30 > 5:
#            	DISPLAYSURF.blit(textSurfaceObj1, textRectObj1)
#        else:
#            DISPLAYSURF.blit(textSurfaceObj1, textRectObj1)
            
#        fontObjdot = pygame.font.Font('Capsuula.ttf', 150)
#        textSurfaceObjdot = fontObjdot.render(":", True, [255, 255, 255])
#        textRectObjdot = textSurfaceObjdot.get_rect()
#        textRectObjdot.center = [screenX / 2 - 90, screenY / 2]
#        if (blink == 1):
#            if countsec % 30 > 5:
#            	DISPLAYSURF.blit(textSurfaceObjdot, textRectObjdot)
#        else:
#        	DISPLAYSURF.blit(textSurfaceObjdot, textRectObjdot)
        
        fontObj2 = pygame.font.Font('OpenSans-Light.ttf', 200)
        textSurfaceObj2 = fontObj2.render(showMinute, True, [255, 255, 255])
        textRectObj2 = textSurfaceObj2.get_rect()
        textRectObj2.center = [screenX / 2 - 150, screenY / 2]
        if (blink == 1) or (select == 2 and start == 0):
            if countsec % 30 > 5:
            	DISPLAYSURF.blit(textSurfaceObj2, textRectObj2)
        else:
            DISPLAYSURF.blit(textSurfaceObj2, textRectObj2)
            
        fontObjdot2 = pygame.font.Font('OpenSans-Light.ttf', 200)
        textSurfaceObjdot2 = fontObjdot2.render(":", True, [255, 255, 255])
        textRectObjdot2 = textSurfaceObjdot2.get_rect()
        textRectObjdot2.center = [screenX / 2 , screenY / 2]
        if start == 1:
            if countsec > 25:
            	DISPLAYSURF.blit(textSurfaceObjdot2, textRectObjdot2)
        elif (blink == 1):
            if countsec % 30 > 5:
            	DISPLAYSURF.blit(textSurfaceObjdot2, textRectObjdot2)
        else:
            DISPLAYSURF.blit(textSurfaceObjdot2, textRectObjdot2)
        
        fontObj3 = pygame.font.Font('OpenSans-Light.ttf', 200)
        textSurfaceObj3 = fontObj3.render(showSecond, True, [255, 255, 255])
        textRectObj3 = textSurfaceObj3.get_rect()
        textRectObj3.center = [screenX / 2 + 150, screenY / 2]
        if (blink == 1) or (select == 3 and start == 0) :
            if countsec % 30 > 5:
            	DISPLAYSURF.blit(textSurfaceObj3, textRectObj3)
        else:
            DISPLAYSURF.blit(textSurfaceObj3, textRectObj3)

        # TIC TOC
        pygame.display.flip()
        pygame.display.update()
        fpsClock.tick(FPS)
        

if __name__=="__main__":
    main()
